# Lab 1

## Together

* man git

* Generate SSH key and upload the pubkey to gitlab.com (`ssh-keygen -b 4096`)

* git config (--global --system --local)

* git init

* `git clone` this repo (explain https/ssh)

* git remote (only origin, upstream next time)

* Try pushing to https:// remote

* git add mv rm

* git status


## Exercise

1. Generate your SSH key and add the public key to your github.com account.

2. Configure Git with your user name and email.

3. Initialize a new Git repository on your local machine.

4. Clone a remote repository using both HTTPS and SSH protocols.

5. Create, move, and remove files within the repository.

6. Check the status of your repository and see the changes you've made.
